package com.wontak.camerafilter.domain.interactors;

import com.wontak.camerafilter.domain.repositories.GithubRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetUserRepositoriesUseCaseTest {

    private GetUserRepositoriesUseCase getUserRepositoriesUseCase;

    @Mock private GithubRepository mockGithubRepository;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(callable -> Schedulers.trampoline());
        getUserRepositoriesUseCase = new GetUserRepositoriesUseCase(mockGithubRepository);
    }

    @Test
    public void testUseCaseObservable() {
        getUserRepositoriesUseCase.buildUseCaseObservable(anyString());

        verify(mockGithubRepository).getUserRepositories(anyString());
        verifyNoMoreInteractions(mockGithubRepository);
    }
}