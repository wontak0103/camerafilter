precision highp float;

uniform vec3        iResolution;
uniform float       iGlobalTime;
uniform sampler2D   iChannel0;
varying vec2        texCoord;

vec2 aberration(vec2 uv, float k)
{
    float r2 = pow(uv.x - 0.5, 2.0) + pow(uv.y - 0.5, 2.0);
    float f = 1.0 + r2 * k;
    return vec2(f * (uv - 0.5) + 0.5);
}

void main()
{
   vec2 uv = texCoord.xy;
   float colorRed = texture2D(iChannel0, aberration(uv, -0.1)).r;
   float colorGreen = texture2D(iChannel0, uv).g;
   float colorBlue = texture2D(iChannel0, aberration(uv, -0.05)).b;
   gl_FragColor = vec4(colorRed, colorGreen, colorBlue, 1.0);
}