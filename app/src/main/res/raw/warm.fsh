precision mediump float;

uniform vec3                iResolution;
uniform float               iGlobalTime;
uniform sampler2D           iChannel0;
varying vec2                texCoord;

vec3 randomGradientWarm(in vec2 texCoord)
{
    vec3 color;
    vec3 purple = vec3(180./255., 151./255., 202./255.);
    vec3 pink = vec3(213./255., 66./255., 108./255.);

  	color.r = texCoord.y * (purple.r - pink.r) + pink.r;
  	color.g = texCoord.y * (purple.g - pink.g) + pink.g;
  	color.b = texCoord.x * (purple.b - pink.b) + pink.b;

	return color;
}

vec3 blend(in vec3 target, in vec3 mask, in float alpha)
{
    return 1.0 - (1.0 - (mask * alpha)) * (1.0 - target);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
	vec3 tex = texture2D(iChannel0, fragCoord).rgb;
	vec3 mask = randomGradientWarm(fragCoord);

	vec3 color = blend(tex, mask, 0.4);
    fragColor = vec4(color, 1.0);
}

void main()
{
    mainImage(gl_FragColor, texCoord);
}