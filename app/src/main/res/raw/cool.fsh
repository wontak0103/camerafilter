precision mediump float;

uniform vec3                iResolution;
uniform float               iGlobalTime;
uniform sampler2D           iChannel0;
varying vec2                texCoord;

vec3 randomGradientCool(in vec2 texCoord)
{
    vec3 color;
    vec3 purple = vec3(180./255., 151./255., 202./255.);
    vec3 mint = vec3(28./255., 169./255., 178./255.);

    color.r = texCoord.y * (purple.r - mint.r) + mint.r;
    color.g = texCoord.x * (purple.g - mint.g) + mint.g;
    color.b = texCoord.x * (purple.b - mint.b) + mint.b;

    return color;
}

vec3 blend(in vec3 target, in vec3 mask, in float alpha)
{
    return 1.0 - (1.0 - (mask * alpha)) * (1.0 - target);
}

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
	vec3 tex = texture2D(iChannel0, fragCoord).rgb;
	vec3 mask = randomGradientCool(fragCoord);

	vec3 color = blend(tex, mask, 0.4);
    fragColor = vec4(color, 1.0);
}

void main()
{
    mainImage(gl_FragColor, texCoord);
}