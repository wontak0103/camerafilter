precision mediump float;

uniform vec3        iResolution;
uniform float       iGlobalTime;
uniform sampler2D   iChannel0;
varying vec2        texCoord;

void make_kernel(inout vec4 n[9])
{
	float w = 1.0 / iResolution.x;
	float h = 1.0 / iResolution.y;

	n[0] = texture2D(iChannel0, texCoord + vec2( -w, -h));
	n[1] = texture2D(iChannel0, texCoord + vec2(0.0, -h));
	n[2] = texture2D(iChannel0, texCoord + vec2(  w, -h));
	n[3] = texture2D(iChannel0, texCoord + vec2( -w, 0.0));
	n[4] = texture2D(iChannel0, texCoord);
	n[5] = texture2D(iChannel0, texCoord + vec2(  w, 0.0));
	n[6] = texture2D(iChannel0, texCoord + vec2( -w, h));
	n[7] = texture2D(iChannel0, texCoord + vec2(0.0, h));
	n[8] = texture2D(iChannel0, texCoord + vec2(  w, h));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
	vec4 n[9];

    make_kernel(n);

    vec4 h = n[2] + n[5] + n[8] - (n[0] + n[3] + n[6]);
    vec4 v = n[0] + n[1] + n[2] - (n[6] + n[7] + n[8]);
    vec4 color = sqrt((h * h) + (v * v));

    fragColor = vec4(1.0 - color.rgb, 1.0);
}

void main()
{
    mainImage(gl_FragColor, texCoord);
}
