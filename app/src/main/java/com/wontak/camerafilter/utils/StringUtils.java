package com.wontak.camerafilter.utils;

public class StringUtils {

    public static boolean isNullOrEmpty(CharSequence charSequence) {
        return (charSequence == null) || (charSequence.length() <= 0);
    }
}
