package com.wontak.camerafilter.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.wontak.camerafilter.R;
import com.wontak.camerafilter.presentation.ui.listeners.CameraView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FiltersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements CameraView.RecyclerViewClickListener {

    private List<MenuItem> items = new ArrayList();
    private CameraView.View view;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.image_icon)
        SimpleDraweeView iconImage;

        @BindView(R.id.label_name)
        TextView nameLabel;

        private CameraView.RecyclerViewClickListener listener;

        public ViewHolder(View v, CameraView.RecyclerViewClickListener listener) {
            super(v);

            ButterKnife.bind(this, v);
            v.setOnClickListener(this);
            this.listener = listener;
        }

        public void setup(MenuItem item) {
            nameLabel.setText(item.getTitle());
        }

        @Override
        public void onClick(View v) {
            listener.onViewClick(getAdapterPosition());
        }
    }

    public FiltersAdapter(Context context, CameraView.View view) {
        this.view = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_filter, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MenuItem item = items.get(position);
        ((ViewHolder) holder).setup(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewClick(int index) {
        view.onFilterClick(items.get(index));
    }

    public void addNewItems(@NonNull List<MenuItem> items) {
        // clean up old data
        if (items != null)
            this.items.clear();

        this.items.addAll(items);

        notifyDataSetChanged();
    }
}
