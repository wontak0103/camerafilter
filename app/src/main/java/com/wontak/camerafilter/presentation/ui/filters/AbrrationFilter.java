package com.wontak.camerafilter.presentation.ui.filters;

import android.content.Context;
import android.opengl.GLES20;

import com.wontak.camerafilter.R;
import com.wontak.camerafilter.utils.OpenGLUtils;

public class AbrrationFilter extends CameraFilter {

    private int program;

    public AbrrationFilter(Context context) {
        super(context);

        // Build shaders
        program = OpenGLUtils.buildProgram(context, R.raw.vertext, R.raw.aberration);
    }

    @Override
    public void onDraw(int cameraTexId, int canvasWidth, int canvasHeight) {
        setupShaderInputs(program,
                new int[]{canvasWidth, canvasHeight},
                new int[]{cameraTexId},
                new int[][]{});
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }
}
