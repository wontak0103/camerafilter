package com.wontak.camerafilter.presentation.ui.listeners;

import android.view.MenuItem;

public interface CameraView {

    interface RecyclerViewClickListener {

        void onViewClick(int index);
    }

    interface View {

        void onFilterClick(MenuItem item);
    }
}
