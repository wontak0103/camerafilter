package com.wontak.camerafilter.presentation.ui.activities;

import android.Manifest;
import android.os.Bundle;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wontak.camerafilter.R;
import com.wontak.camerafilter.base.BaseActivity;
import com.wontak.camerafilter.presentation.ui.fragments.CameraFragment;

public class MainActivity extends BaseActivity {

    private CameraFragment fragment;

    private RxPermissions rxPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rxPermissions = new RxPermissions(this);
        hideStatusBar();
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkPermissions();
    }

    private void checkPermissions() {
        rxPermissions.request(Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) {
                        initializeFragment();
                    } else {
                        checkPermissions();
                    }
                });
    }

    private void initializeFragment() {
        fragment = CameraFragment.newInstance();
        addFragment(R.id.layout_content, fragment);
    }
}
