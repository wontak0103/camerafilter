package com.wontak.camerafilter.presentation.ui.fragments;

import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.transition.AutoTransition;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.wontak.camerafilter.R;
import com.wontak.camerafilter.base.BaseFragment;
import com.wontak.camerafilter.presentation.ui.adapters.FiltersAdapter;
import com.wontak.camerafilter.presentation.ui.filters.CameraRenderer;
import com.wontak.camerafilter.presentation.ui.listeners.CameraView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CameraFragment extends BaseFragment implements CameraView.View {

    private static final int SIZE_TAKE_BUTTON_DP = 64;

    @BindView(R.id.texture_view)
    TextureView textureView;

    @BindView(R.id.layout_adjust)
    ConstraintLayout adjustLayout;

    @BindView(R.id.rv_filters)
    RecyclerView filtersRecyclerView;

    @BindView(R.id.button_take)
    Button takeButton;

    private CameraRenderer renderer;
    private FiltersAdapter adapter;

    private boolean showFilters;

    private Unbinder unbinder;

    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        unbinder = ButterKnife.bind(this, view);

        initializeRecyclerView();

        return view;
    }

    private void initializeRecyclerView() {
        adapter = new FiltersAdapter(getContext(), this);
        adapter.addNewItems(inflateMenuItems(R.menu.filter));

        filtersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        filtersRecyclerView.setAdapter(adapter);
    }

    private List<MenuItem> inflateMenuItems(@MenuRes int menuResId) {
        List<MenuItem> toReturn = new ArrayList();
        PopupMenu popup = new PopupMenu(getContext(), null);
        popup.inflate(menuResId);
        for (int i=0; i<popup.getMenu().size(); i++) {
            toReturn.add(popup.getMenu().getItem(i));
        }
        return toReturn;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupCameraPreviewView();
    }

    private void setupCameraPreviewView() {
        renderer = new CameraRenderer(getContext());
        textureView.setSurfaceTextureListener(renderer);
        textureView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> renderer.onSurfaceTextureSizeChanged(null, v.getWidth(), v.getHeight()));
    }

    @OnClick(R.id.button_show_filters)
    public void onShowFiltersClick() {
        Transition transition = new AutoTransition();
        transition.setDuration(200);
        TransitionManager.beginDelayedTransition(adjustLayout, transition);
        showFilters = !showFilters;

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(adjustLayout);

        constraintSet.setVisibility(R.id.rv_filters, showFilters ? View.VISIBLE : View.INVISIBLE);

        constraintSet.constrainWidth(R.id.button_take, showFilters ? dpToPx(SIZE_TAKE_BUTTON_DP) * 2 / 3 : dpToPx(SIZE_TAKE_BUTTON_DP));
        constraintSet.constrainHeight(R.id.button_take, showFilters ? dpToPx(SIZE_TAKE_BUTTON_DP) * 2 / 3 : dpToPx(SIZE_TAKE_BUTTON_DP));

        constraintSet.applyTo(adjustLayout);
    }

    private int dpToPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @Override
    public void onFilterClick(MenuItem item) {
        renderer.setSelectedFilter(item.getItemId());
    }
}
