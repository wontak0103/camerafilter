package com.wontak.camerafilter.di.components;

import com.wontak.camerafilter.base.BaseActivity;
import com.wontak.camerafilter.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity activity);
}
