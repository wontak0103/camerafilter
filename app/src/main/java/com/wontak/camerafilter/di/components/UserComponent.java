package com.wontak.camerafilter.di.components;

import com.wontak.camerafilter.di.PerActivity;
import com.wontak.camerafilter.di.modules.ActivityModule;
import com.wontak.camerafilter.di.modules.UserModule;
import com.wontak.camerafilter.presentation.ui.fragments.CameraFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UserModule.class})
public interface UserComponent {

    void inject(CameraFragment fragment);
}
