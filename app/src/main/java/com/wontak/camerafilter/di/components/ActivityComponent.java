package com.wontak.camerafilter.di.components;

import android.app.Activity;

import com.wontak.camerafilter.di.PerActivity;
import com.wontak.camerafilter.di.modules.ActivityModule;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    Activity activity();
}
