package com.wontak.camerafilter.di;

public interface HasComponent<T> {

    T getComponent();
}
