package com.wontak.camerafilter;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.wontak.camerafilter.di.components.ApplicationComponent;
import com.wontak.camerafilter.di.components.DaggerApplicationComponent;
import com.wontak.camerafilter.di.modules.ApplicationModule;

public class BaseApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeFresco();
        initializeInjector();
    }

    private void initializeInjector() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void initializeFresco() {
        Fresco.initialize(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
